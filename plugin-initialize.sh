#!/bin/sh
P=$(curl --write-out "%{http_code}\n" --silent --output /dev/null --request GET \
  --url http://127.0.0.1:8001/plugins/d6ddbcbe-4207-432d-a9bc-ae5e14a234c1)
if [[ $P -ne 200 ]]
then
    while :
    do
      P=$(curl --write-out "%{http_code}\n" --silent --output /dev/null --request POST \
        --url http://127.0.0.1:8001/plugins \
        --header 'Content-Type: application/json' \
        --data '{"name":"tcp-log","config":{"host": "amp-apiproject-logstash-service","port": 1514},"id":"d6ddbcbe-4207-432d-a9bc-ae5e14a234c1"}')
      if [[ $P -eq 201 ]]
      then
        exit 0
      fi
    done
fi
